//
//  ViewController.swift
//  colorOnTap
//
//  Created by User Mac on 13/08/2018.
//  Copyright © 2018 User Mac. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var tapView: UIView!
    
    var tapAction = UITapGestureRecognizer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        tapAction = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        tapView.addGestureRecognizer(tapAction)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        if sender.state == .ended {
            self.tapView.backgroundColor = getRandomColor()
        }
    }
    
    func getRandomColor() ->UIColor {
        return UIColor(red: CGFloat(drand48()), green: CGFloat(drand48()), blue: CGFloat(drand48()), alpha: 1.0)
    }
}

